close all;
clear all;
clc;



format long;

ADC_12bit_Ratio = 3.3 / (2^12);
R = 680;
Current_Ratio = 0.0024;

C_Data2Current = ADC_12bit_Ratio*1000 / (R*Current_Ratio);

C_Data2Current