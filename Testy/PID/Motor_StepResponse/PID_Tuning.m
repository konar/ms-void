close all;
clear all;
clc;

data=readtable('005_700.txt');
output = table2array(data);

input_val = 0.7 * 16.8 * 16.617;
size = length(output);
input = linspace(input_val, input_val, size)';

in_data = iddata(output, input, 0.001);
motor_tf = tfest(in_data, 1, 0, 'Ts', 0.001);

[y2, t2] = step(motor_tf(2));
y2 = y2*input_val;

[y3, t3] = step(motor_tf(3));
y3 = y3*input_val;

figure(1);
subplot(1,2,1); hold on; grid on; grid minor;
scatter(output(:,1), output(:,2), 10 ,'r','filled');
title('Lewy silnik');
xlabel('Czas [ms]');
ylabel('Predkosc silnika [RPS]');

plot(t2*1000,y2,'b','LineWidth',2);
axis([0 700 0 300]);

legend('Dane z pomiaru','Wyznaczona charakterystyka');

subplot(1,2,2); hold on; grid on; grid minor;
scatter(output(:,1), output(:,3), 10, 'g','filled');
title('Prawy silnik');
xlabel('Czas [ms]');
ylabel('Predkosc silnika [RPS]');

plot(t3*1000,y3,'b','LineWidth',2);
axis([0 700 0 300]);

legend('Dane z pomiaru','Wyznaczona charakterystyka');

sim('PID_Tuning_sim.slx');

figure(4);
subplot(1,2,1);
title('Lewy silnik');
plot(T,Out1, 'g');
hold on; grid on; grid minor;
plot(t2,y2,'-r');
xlabel('Czas [s]');
ylabel('Predkosc silnika [RPS]');

subplot(1,2,1);
title('Prawy silnik');
plot(T,Out2, 'g');
hold on; grid on; grid minor;
plot(t3,y3,'-r');
xlabel('Czas [s]');
ylabel('Predkosc silnika [RPS]');


