close all;
clear all;
clc;

% 008 - (L) Testowany bez przekladni (R) Sprawny
% 009 - (L) Testowany z przekladnia bez felgi (R) Sprawny
% 010 - (L) Testowany z felg� (R) Sprawny

data1=readtable('011');
output = table2array(data1);

time_ms = output(:,1);
leftMotorCurrent = output(:,2);
leftMotorCurrentFiltered = output(:,3);
leftMotorVelocity = -output(:,4);
leftMotorVelocityFiltered = -output(:,5);

rightMotorCurrent = output(:,6);
rightMotorCurrentFiltered = output(:,7);
rightMotorVelocity = output(:,8);
rightMotorVelocityFiltered = output(:,9);

figure(1);
subplot(2,2,1); hold on; grid on; grid minor;
scatter(time_ms, leftMotorVelocity, 5, 'filled', 'r'); 
scatter(time_ms, leftMotorVelocityFiltered, 15, 'filled', 'g');
ylabel('Motor velocity [RPS]');
xlabel('Time [ms]');
title('Left motor velocity');
legend('Raw', 'Filtered');
axis([0 500 0 300]);

subplot(2,2,2); hold on; grid on; grid minor;
scatter(time_ms, rightMotorVelocity, 5, 'filled', 'r'); 
scatter(time_ms, rightMotorVelocityFiltered, 15, 'filled', 'g');
ylabel('Motor velocity [RPS]');
xlabel('Time [ms]');
title('Right motor velocity');
legend('Raw', 'Filtered');
% axis([0 500 0 300]);

subplot(2,2,3); hold on; grid on; grid minor;
scatter(time_ms, leftMotorCurrent, 5, 'filled', 'r'); 
scatter(time_ms, leftMotorCurrentFiltered, 15, 'filled', 'g');
ylabel('Motor velocity [mA]');
xlabel('Time [ms]');
title('Left motor current');
legend('Raw', 'Filtered');
axis([0 500 0 800]);

subplot(2,2,4); hold on; grid on; grid minor;
scatter(time_ms, rightMotorCurrent, 5, 'filled', 'r'); 
scatter(time_ms, rightMotorCurrentFiltered, 15, 'filled', 'g');
ylabel('Motor current [mA]');
xlabel('Time [ms]');
title('Right motor current');
legend('Raw', 'Filtered');
axis([0 500 0 800]);
