close all;
clear all;
clc;

% data=readtable('D:\Git-Projekty\project-supernova\Testy\PID\STM_Studio_Data\log1.txt');
% Watafak = table2array(data);
% 
% return;
% 
% data=readtable('c_sens002.txt');
% D04 = table2array(data);
% 
% data=readtable('c_sens002.txt');
% D05 = table2array(data);
% 
% data=readtable('c_sens003.txt');
% D06 = table2array(data);
% 
% data=readtable('c_sens007.txt');
% D07 = table2array(data);
% 
% 
% 
% % for i=1:548
% %     myString = data(1,1);
% %     myString( isspace(myString) ) = [];
% %     data(i,1) = str2int(myString);
% % end
% 
% figure(1);
% 
% 
% subplot(1,2,1)
% 
% 
% scatter(D07(:,1), D07(:,2), 15, 'filled', 'r'); hold on;grid on; grid minor;
% scatter(D06(:,1), D06(:,2), 15, 'filled', 'g');
% ylabel('Current');
% xlabel('PWM');
% title('Lewy silnik');
% 
% subplot(1,2,2)
% scatter(D07(:,1), D07(:,3), 15, 'filled', 'r'); hold on; grid on; grid minor;
% scatter(D06(:,1), D06(:,3), 15, 'filled', 'g');
% ylabel('Current');
% xlabel('PWM');
% title('Prawy silnik');
% 

data1=readtable('c_sens_fil008.txt');
output1 = table2array(data1);

scatter(output1(:,1), output1(:,3), 15, 'filled', 'r'); hold on; grid on; grid minor;
scatter(output1(:,1), output1(:,2), 8, 'filled', 'g');
ylabel('Current [mA]');
xlabel('Time [ms]');
title('Current measurement with and without alpha filter');
legend('Filtered data', 'Raw data');
axis([0 1000 0 800]);