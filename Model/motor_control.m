clear all;
close all;
clc;

% Motor parameters
nominal_voltage = 9; % [V]
term_resistance = 10.6; % [Ohm]
effiency_max = 80; % [%]

speed_noload = 10000 / 60; % [s^-1 (Hz)]
speed_noload_rpm = 10000;
current_noload = 9 * 10^-3; % [A]

torque_stall = 7.18 * 10^-3; % [Nm]
torque_friction = 0.08; % [mNm]

speed_const = 1117 / 60; % [Hz / V]
backEMF_const = 0.895 * 60; % [mV*Hz]
torque_const = 8.55; % [mNm/A]
current_const = 0.117; % [A/mNm]
slope_nM_curve = 1380 / 60; % [Hz/mNm]
rotor_inductance = 230 * 10^-3; % [H]

mechanical_time_constant = 8.3; % [ms]
rotor_intertia = 0.57; % [g*cm^2]

gear_ratio = 1/19;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
supply_voltage = 9;
% velocity = speed_constant * voltage
% voltage = vel / speed_constant1

velocity2voltage = 1/speed_const;


% Wysoko�� skoku
step_value = 100;

J = 0.01; % [kg*m^2]
b = 0.1; % [N*m*s]
Ke = 0; % speed const [V*Hz/rad]
Kt = 0; % Current const [N*m/A] 
K = 0.01; % 
R = 1; % Electric resistance [Ohm]
L = 0.5; % Electric inductance [H]
s = tf('s');
P_motor = K/((J*s+b)*(L*s+R)+K^2)


% sim('motor_control_mdl.slx');

% figure(1);
% plot(t,vel_m,'r');
% title('Odpowied� skokowa [pr�dko��]');
% xlabel('t[ms]');
% ylabel('Velocity [RPS]');