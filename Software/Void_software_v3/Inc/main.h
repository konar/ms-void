/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  ** This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether 
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * COPYRIGHT(c) 2018 STMicroelectronics
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H__
#define __MAIN_H__

/* Includes ------------------------------------------------------------------*/
#include "stm32f4xx_hal.h"

/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Private define ------------------------------------------------------------*/

#define DM1_Pin GPIO_PIN_13
#define DM1_GPIO_Port GPIOC
#define P1_Pin GPIO_PIN_14
#define P1_GPIO_Port GPIOC
#define P2_Pin GPIO_PIN_15
#define P2_GPIO_Port GPIOC
#define KTIR_LM_Pin GPIO_PIN_0
#define KTIR_LM_GPIO_Port GPIOC
#define KTIR_L_Pin GPIO_PIN_1
#define KTIR_L_GPIO_Port GPIOC
#define KTIR_R_Pin GPIO_PIN_2
#define KTIR_R_GPIO_Port GPIOC
#define KTIR_RM_Pin GPIO_PIN_3
#define KTIR_RM_GPIO_Port GPIOC
#define MT1_A_Pin GPIO_PIN_0
#define MT1_A_GPIO_Port GPIOA
#define MT1_B_Pin GPIO_PIN_1
#define MT1_B_GPIO_Port GPIOA
#define BATT_SENS_Pin GPIO_PIN_2
#define BATT_SENS_GPIO_Port GPIOA
#define MT_C_SENS_L_Pin GPIO_PIN_4
#define MT_C_SENS_L_GPIO_Port GPIOA
#define MT_C_SENS_R_Pin GPIO_PIN_5
#define MT_C_SENS_R_GPIO_Port GPIOA
#define DRDY_M_Pin GPIO_PIN_6
#define DRDY_M_GPIO_Port GPIOA
#define INT_M_Pin GPIO_PIN_7
#define INT_M_GPIO_Port GPIOA
#define INT1_A_G_Pin GPIO_PIN_4
#define INT1_A_G_GPIO_Port GPIOC
#define INT2_A_G_Pin GPIO_PIN_5
#define INT2_A_G_GPIO_Port GPIOC
#define MT_PWM_R_Pin GPIO_PIN_0
#define MT_PWM_R_GPIO_Port GPIOB
#define DEN_A_G_Pin GPIO_PIN_1
#define DEN_A_G_GPIO_Port GPIOB
#define IN12_Pin GPIO_PIN_2
#define IN12_GPIO_Port GPIOB
#define SCL_I_Pin GPIO_PIN_10
#define SCL_I_GPIO_Port GPIOB
#define SDA_I_Pin GPIO_PIN_11
#define SDA_I_GPIO_Port GPIOB
#define IN22_Pin GPIO_PIN_12
#define IN22_GPIO_Port GPIOB
#define MT_PWM_L_Pin GPIO_PIN_13
#define MT_PWM_L_GPIO_Port GPIOB
#define IN21_Pin GPIO_PIN_14
#define IN21_GPIO_Port GPIOB
#define IN11_Pin GPIO_PIN_15
#define IN11_GPIO_Port GPIOB
#define D6_Pin GPIO_PIN_6
#define D6_GPIO_Port GPIOC
#define INT6_Pin GPIO_PIN_7
#define INT6_GPIO_Port GPIOC
#define INT6_EXTI_IRQn EXTI9_5_IRQn
#define INT5_Pin GPIO_PIN_8
#define INT5_GPIO_Port GPIOC
#define INT5_EXTI_IRQn EXTI9_5_IRQn
#define VL_SDA_Pin GPIO_PIN_9
#define VL_SDA_GPIO_Port GPIOC
#define VL_SCL_Pin GPIO_PIN_8
#define VL_SCL_GPIO_Port GPIOA
#define INT4_Pin GPIO_PIN_9
#define INT4_GPIO_Port GPIOA
#define INT4_EXTI_IRQn EXTI9_5_IRQn
#define INT2_Pin GPIO_PIN_10
#define INT2_GPIO_Port GPIOA
#define INT2_EXTI_IRQn EXTI15_10_IRQn
#define INT1_Pin GPIO_PIN_11
#define INT1_GPIO_Port GPIOA
#define INT1_EXTI_IRQn EXTI15_10_IRQn
#define INT3_Pin GPIO_PIN_12
#define INT3_GPIO_Port GPIOA
#define INT3_EXTI_IRQn EXTI15_10_IRQn
#define START_Pin GPIO_PIN_15
#define START_GPIO_Port GPIOA
#define UART_TX_Pin GPIO_PIN_10
#define UART_TX_GPIO_Port GPIOC
#define UART_RX_Pin GPIO_PIN_11
#define UART_RX_GPIO_Port GPIOC
#define D1_Pin GPIO_PIN_12
#define D1_GPIO_Port GPIOC
#define D2_Pin GPIO_PIN_2
#define D2_GPIO_Port GPIOD
#define D3_Pin GPIO_PIN_3
#define D3_GPIO_Port GPIOB
#define MT2_A_Pin GPIO_PIN_4
#define MT2_A_GPIO_Port GPIOB
#define MT2_B_Pin GPIO_PIN_5
#define MT2_B_GPIO_Port GPIOB
#define D4_Pin GPIO_PIN_6
#define D4_GPIO_Port GPIOB
#define D5_Pin GPIO_PIN_7
#define D5_GPIO_Port GPIOB
#define DM3_Pin GPIO_PIN_8
#define DM3_GPIO_Port GPIOB
#define DM2_Pin GPIO_PIN_9
#define DM2_GPIO_Port GPIOB

/* ########################## Assert Selection ############################## */
/**
  * @brief Uncomment the line below to expanse the "assert_param" macro in the 
  *        HAL drivers code
  */
/* #define USE_FULL_ASSERT    1U */

/* USER CODE BEGIN Private defines */

#ifdef __cplusplus
 extern "C" {
#endif

int main(void);

#ifdef __cplusplus
}
#endif


 /* USER CODE END Private defines */

#ifdef __cplusplus
 extern "C" {
#endif
void _Error_Handler(char *, int);

#define Error_Handler() _Error_Handler(__FILE__, __LINE__)
#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H__ */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
