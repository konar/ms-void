/*
 * Gpio.h
 *
 *  Created on: 02.09.2018
 *      Author: olek1
 */

#ifndef GENERALPURPOSEIO_H_
#define GENERALPURPOSEIO_H_

#include "stm32f4xx_hal.h"

typedef enum {
	input 		= 0,
	output 		= 1,
	alternate 	= 2,
	analog		= 3
} gpioMode;

typedef enum {
	unknown = -1,
	off		= 0,
	on 		= 1
} gpioState;



class generalPurposeIO {
	GPIO_TypeDef* bus;
	uint16_t pin;
	bool enabled;
	gpioState state;
	gpioMode mode;

public:
	generalPurposeIO();
	virtual ~generalPurposeIO();

	gpioState toggle();
	gpioState turnOn();
	gpioState turnOff();
};

#endif /* GENERALPURPOSEIO_H_ */
