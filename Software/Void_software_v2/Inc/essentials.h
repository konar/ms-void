#ifndef ESSENTIALS_H
#define ESSENTIALS_H

#include "stm32f4xx_hal.h"
#include "defines.h"
//#include "../Src/sensors/imu.h"

/*
 * Opoznienie wy��czenia silnik�w podczas hamowania ( ustawienia PWM duty = 0 )
 */

void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim);

extern volatile uint8_t speedSelect;
extern volatile uint8_t buttonLock;
extern volatile uint16_t buttonClickTimeElapsed;

extern volatile uint8_t imu_data_ready;
extern volatile uint16_t imu_count;
extern volatile uint8_t imu_error;

extern volatile uint32_t perf_count;
extern volatile float perf_avg_ms;
extern volatile uint32_t perf_meas_ms;

// Flagi
extern volatile uint8_t Cycle_time_elapsed;
extern volatile uint8_t Cycle_finished;
extern volatile Error_t Runtime_Error;
extern volatile uint16_t Runtime_Error_Sum;

// Settings
extern uint8_t StartRot;
extern uint8_t Tactic;
extern uint8_t FrontFight;


void displaySelect(uint8_t number);

#endif /* ESSENTIALS_H */
