/*
 * defines.h
 *
 *  Created on: 13 sty 2017
 *      Author: Aleksander Sil
 */

#ifndef DEFINES_H_
#define DEFINES_H_

#include "stm32f4xx_hal.h"
#include "math.h"

#define DEBUG 1
#define TOURNAMENT 1 // 0 - mozliwe skalowanie predkosci

#define Delay_Break 50
#define WaitButton 200

#define ADC_12BIT_VOLTAGE_RATIO 0.000806f

typedef enum {
	NO_ERROR = 0,
	TIMEOUT
} Error_t;

extern ADC_HandleTypeDef hadc1;
extern ADC_HandleTypeDef hadc2;
extern ADC_HandleTypeDef hadc3;
extern DMA_HandleTypeDef hdma_adc2;
extern DMA_HandleTypeDef hdma_adc3;

extern I2C_HandleTypeDef hi2c2;
extern I2C_HandleTypeDef hi2c3;

extern TIM_HandleTypeDef htim1;
extern TIM_HandleTypeDef htim2;
extern TIM_HandleTypeDef htim3;
extern TIM_HandleTypeDef htim6;
extern TIM_HandleTypeDef htim7;

extern UART_HandleTypeDef huart3;


#endif /* DEFINES_H_ */
