#ifndef COMBAT_H
#define COMBAT_H

#include "../Src/state/state.h"



void Wait5Sec();
void Find_and_kill_raw();
void StraightRide();


void WhiteLineReaction(float *ScaleL, float *ScaleR, uint8_t *FLAG_RunFromLine);
void TargetEnemy(float ScaleL, float ScaleR);
void RunFromLine(int16_t* Counter, uint8_t* FLAG_RunFromLine);
void Start();

#endif
