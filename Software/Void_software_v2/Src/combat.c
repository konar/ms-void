#include "combat.h"

int i;



/*
 *
 * ALGORYTMY WALKI
 *
 */
volatile uint16_t SideFight_Counter = 0;
volatile uint16_t Start_Counter = 0;
uint8_t Started = 0;

uint8_t frontFightMaxForwardSpeedTable[7] = {40, 60, 80, 100, 120, 160, 200};
uint8_t frontFightMaxForwardSpeed = 0;

uint8_t IGNORE_WHITE_LINE = 0;

void Wait5Sec()
{
	int h=0;
	for(h=0; h<5; h++){
		HAL_GPIO_WritePin(IK.diodyBus[h], IK.diodyPin[h], GPIO_PIN_RESET);
	}
	HAL_GPIO_WritePin(IK.diodyBus[1], IK.diodyPin[1], GPIO_PIN_SET);

	for(h=0; h<5; h++)
	{
		HAL_GPIO_WritePin(IK.diodyBus[h], IK.diodyPin[h], GPIO_PIN_SET);
		HAL_Delay(1000);
	}
}

void Find_and_kill_raw()
{
	float ScaleR = 1;
	float ScaleL = 1;
	uint8_t FLAG_RunFromLine = 0;

	int16_t Counter = 0;

	while(HAL_GPIO_ReadPin(START_GPIO_Port, START_Pin))
	{
		State_Update(1,2);
		WhiteLineReaction(&ScaleL, &ScaleR, &FLAG_RunFromLine);
		if(!FLAG_RunFromLine){
			TargetEnemy(1,1);
		}
		else
		{
			RunFromLine(&Counter, &FLAG_RunFromLine);
		}
	}
}

void StraightRide()
{
	float ScaleR = 1;
	float ScaleL = 1;
	uint8_t FLAG_RunFromLine = 0;
	int16_t Counter = 0;

	while(HAL_GPIO_ReadPin(START_GPIO_Port, START_Pin))
	{
		State_Update(2,2);
		if(!IGNORE_WHITE_LINE)	WhiteLineReaction(&ScaleL, &ScaleR, &FLAG_RunFromLine);
		if(!FLAG_RunFromLine)	Velocity_SetTarget(120,120);
		else					RunFromLine(&Counter, &FLAG_RunFromLine);
	}

}


void RunFromLine(int16_t* Counter, uint8_t* FLAG_RunFromLine)
{
	uint16_t RunTime = 580;
	if(*FLAG_RunFromLine)
	{
		if(*Counter == 0)
			f_SH_ClearMemory(VLS.MEMORY);
		else if(*Counter < 400)
		{
			if(*FLAG_RunFromLine == 1)	{
				Velocity_SetTarget(-140,-140);
				RunTime = 580;
			}
			else if(*FLAG_RunFromLine == 2)	{
				Velocity_SetTarget(-160,-100);
				RunTime = 500;
			}
			else if(*FLAG_RunFromLine == 3)	{
				Velocity_SetTarget(-100,-160);
				RunTime = 500;
			}
		}
		else if (*Counter < RunTime)
		{ // zmienic tego ifa zeby czas byl <580 w przypadku flag = 1, a mniej w przypadku 2 i 3
			if(VLS.TARGET_DIR > 0)
				Velocity_SetTarget(150,-150);
			else
				Velocity_SetTarget(-150,150);
		}

		else if (!sNone(VLS.STATE))
		{
			*FLAG_RunFromLine = 0;
			*Counter = 0;
		}
		else if (*Counter < 800)
			Velocity_SetTarget(100,100);
		else
		{
			*FLAG_RunFromLine = 0;
			*Counter = 0;
		}
		*Counter = *Counter + 5;
	}
}


/*
 * FLAG_RunFromLine
 * 0 - brak reakcji
 * 1 - uciekaj do tylu
 * 2 - do tylu po luku w prawo + obrot
 * 3 - do tylu po luku w lewo + obrot
 */
void WhiteLineReaction(float *ScaleL, float *ScaleR, uint8_t *FLAG_RunFromLine)
{
	if((KTIRS.Memory[0] && KTIRS.Memory[1]) && sNone(VLS.MEMORY))	{
		*ScaleL = 1;
		*ScaleR = 1;
		*FLAG_RunFromLine = 1;
	}
	else if(KTIRS.Memory[0] && !KTIRS.Memory[1] && sNone(VLS.STATE))	{
		*ScaleL = 1;
		*ScaleR = 1;
		*FLAG_RunFromLine = 2;
	}
	else if(!KTIRS.Memory[0] && KTIRS.Memory[1] && sNone(VLS.STATE))	{
		*ScaleL = 1;
		*ScaleR = 1;
		*FLAG_RunFromLine = 3;
	}
	// dotad jest chyba spoko
	else if(KTIRS.Memory[0] && KTIRS.Memory[1] && (fabsf(VLS.TARGET_DIR) <= 3))	{
		*ScaleL = 1; // przeciwnik przed robotem, ale robot juz 2 czujnikami na linii, wiec powinien uciekac
		*ScaleR = 1; // zastanowic sie nad tym, bo mozliwe, ze robot przeciwnika bedzie mocno nad
		*FLAG_RunFromLine = 1; // supernova i wtedy go nie wypchnie
	}
	else if(KTIRS.Memory[0] && (VLS.TARGET_DIR <= 0)) {
		*ScaleL = 0.8;
		*ScaleR = 1;
	}
	else if (KTIRS.Memory[0] && (VLS.TARGET_DIR > 0))	{
		*ScaleL = 0.6;
		*ScaleR = 0.9;
	}
	else if (KTIRS.Memory[1] && (VLS.TARGET_DIR >= 0))	{
		*ScaleL = 1;
		*ScaleR = 0.8;
	}
	else if	(KTIRS.Memory[1] && (VLS.TARGET_DIR < 0))	{
		*ScaleL = 0.9;
		*ScaleR = 0.6;
	}
	else	{
		*ScaleL = 1;
		*ScaleR = 1;
	}
}


void TargetEnemy(float ScaleL, float ScaleR)
{
	if(!sNone(VLS.MEMORY) && !Started) Started = 1;
	if(Started)	Tactic = 6;

	frontFightMaxForwardSpeed = frontFightMaxForwardSpeedTable[speedSelect];
	/*
	 * VL53L0X - wait for all new readings
	 */



	if(sNone(VLS.MEMORY))
	{
		Start();
	}
	else if(sLeft(VLS.MEMORY) || sLeft_MidL(VLS.MEMORY))
	{
			Velocity_SetTarget(-220*ScaleL, 220*ScaleR);
	}
	else if(sRight(VLS.MEMORY) || sRight_MidR(VLS.MEMORY))
	{
			Velocity_SetTarget(220*ScaleL, -220*ScaleR);
	}
	else if(sLeft_FrontL(VLS.MEMORY))
		Velocity_SetTarget(-160*ScaleL, 200*ScaleR);
	else if(sRight_FrontR(VLS.MEMORY))
		Velocity_SetTarget(200*ScaleL, -160*ScaleR);

	else if(sLeft_MidL_FrontL(VLS.MEMORY))
		Velocity_SetTarget(-100*ScaleL, 200*ScaleR);
	else if(sRight_MidR_FrontR(VLS.MEMORY))
		Velocity_SetTarget(200*ScaleL, -100*ScaleR);

	else if(sFrontL_MidL(VLS.MEMORY))
		Velocity_SetTarget(120*ScaleL, 200*ScaleR);
	else if(sFrontR_MidR(VLS.MEMORY))
		Velocity_SetTarget(200*ScaleL, 120*ScaleR);

	else if(sMidL(VLS.MEMORY))
		Velocity_SetTarget(115*ScaleL, 200*ScaleR);
	else if(sMidR(VLS.MEMORY))
		Velocity_SetTarget(200*ScaleL, 115*ScaleR);

	else if(sFrontL(VLS.MEMORY))
//		Velocity_SetTarget(170*ScaleL, 200*ScaleR);
		Velocity_SetTarget(0.85*frontFightMaxForwardSpeed*ScaleL, frontFightMaxForwardSpeed*ScaleR);
	else if(sFrontR(VLS.MEMORY))
//		Velocity_SetTarget(200*ScaleL, 170*ScaleR);
		Velocity_SetTarget(frontFightMaxForwardSpeed*ScaleL, 0.85*frontFightMaxForwardSpeed*ScaleR);

	else if(sFrontL_MidL_Front_R(VLS.MEMORY))
//		Velocity_SetTarget(200*ScaleL, 220*ScaleR);
		Velocity_SetTarget(0.9*frontFightMaxForwardSpeed*ScaleL, frontFightMaxForwardSpeed*ScaleR);
	else if(sFrontL_MidR_Front_R(VLS.MEMORY))
//		Velocity_SetTarget(220*ScaleL, 200*ScaleR);
		Velocity_SetTarget(frontFightMaxForwardSpeed*ScaleL, 0.9*frontFightMaxForwardSpeed*ScaleR);

	else if(sMidL_MidR(VLS.MEMORY))
//		Velocity_SetTarget(160*ScaleL, 160*ScaleR);
		Velocity_SetTarget(0.8*frontFightMaxForwardSpeed*ScaleL, 0.8*frontFightMaxForwardSpeed*ScaleR);

	else if(sFrontL_MidL_MidR_FrontR(VLS.MEMORY))
//		Velocity_SetTarget(220*ScaleL, 220*ScaleR);
		Velocity_SetTarget(frontFightMaxForwardSpeed*ScaleL, frontFightMaxForwardSpeed*ScaleR);

	else if(sFrontL_FrontR(VLS.MEMORY))
//		Velocity_SetTarget(200*ScaleL, 200*ScaleR);
		Velocity_SetTarget(frontFightMaxForwardSpeed*ScaleL, frontFightMaxForwardSpeed*ScaleR);
	else
		Velocity_SetTarget(-160*ScaleL, 160*ScaleR);
}


void Start()
{

	Velocity_SetTarget(40,40);



//	if(StartRot == 0)
//	{	Velocity_SetTarget(-200,200);	}
//	else
//	{	Velocity_SetTarget(200,-200);	}
}


