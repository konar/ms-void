/*
 * essentials.c
 *
 *  Created on: 3 lis 2016
 *      Author: Aleksander Sil
 */

#include "essentials.h"

uint8_t StartRot = 0;
uint8_t FrontFight = 0;
uint8_t Tactic = 0;

volatile uint8_t speedSelect = 0;
volatile uint8_t buttonLock = 0;
volatile uint16_t buttonClickTimeElapsed = 0;

int i;

volatile uint8_t imu_data_ready = 0;
volatile uint16_t imu_count = 0;
volatile uint8_t imu_error = 0;

volatile uint32_t perf_count = 0;
volatile float perf_avg_ms = 0;
volatile uint32_t perf_meas_ms = 0;


void displaySelect(uint8_t number){
	switch(number){
	case 0:
		HAL_GPIO_WritePin(DM1_GPIO_Port, DM1_Pin, GPIO_PIN_RESET);
		HAL_GPIO_WritePin(DM2_GPIO_Port, DM2_Pin, GPIO_PIN_RESET);
		HAL_GPIO_WritePin(DM3_GPIO_Port, DM3_Pin, GPIO_PIN_RESET);
		break;
	case 1:
		HAL_GPIO_WritePin(DM1_GPIO_Port, DM1_Pin, GPIO_PIN_SET);
		HAL_GPIO_WritePin(DM2_GPIO_Port, DM2_Pin, GPIO_PIN_RESET);
		HAL_GPIO_WritePin(DM3_GPIO_Port, DM3_Pin, GPIO_PIN_RESET);
		break;
	case 2:
		HAL_GPIO_WritePin(DM1_GPIO_Port, DM1_Pin, GPIO_PIN_RESET);
		HAL_GPIO_WritePin(DM2_GPIO_Port, DM2_Pin, GPIO_PIN_SET);
		HAL_GPIO_WritePin(DM3_GPIO_Port, DM3_Pin, GPIO_PIN_RESET);
		break;
	case 3:
		HAL_GPIO_WritePin(DM1_GPIO_Port, DM1_Pin, GPIO_PIN_SET);
		HAL_GPIO_WritePin(DM2_GPIO_Port, DM2_Pin, GPIO_PIN_SET);
		HAL_GPIO_WritePin(DM3_GPIO_Port, DM3_Pin, GPIO_PIN_RESET);
		break;
	case 4:
		HAL_GPIO_WritePin(DM1_GPIO_Port, DM1_Pin, GPIO_PIN_RESET);
		HAL_GPIO_WritePin(DM2_GPIO_Port, DM2_Pin, GPIO_PIN_RESET);
		HAL_GPIO_WritePin(DM3_GPIO_Port, DM3_Pin, GPIO_PIN_SET);
		break;
	case 5:
		HAL_GPIO_WritePin(DM1_GPIO_Port, DM1_Pin, GPIO_PIN_SET);
		HAL_GPIO_WritePin(DM2_GPIO_Port, DM2_Pin, GPIO_PIN_RESET);
		HAL_GPIO_WritePin(DM3_GPIO_Port, DM3_Pin, GPIO_PIN_SET);
		break;
	case 6:
		HAL_GPIO_WritePin(DM1_GPIO_Port, DM1_Pin, GPIO_PIN_RESET);
		HAL_GPIO_WritePin(DM2_GPIO_Port, DM2_Pin, GPIO_PIN_SET);
		HAL_GPIO_WritePin(DM3_GPIO_Port, DM3_Pin, GPIO_PIN_SET);
		break;
	case 7:
		HAL_GPIO_WritePin(DM1_GPIO_Port, DM1_Pin, GPIO_PIN_SET);
		HAL_GPIO_WritePin(DM2_GPIO_Port, DM2_Pin, GPIO_PIN_SET);
		HAL_GPIO_WritePin(DM3_GPIO_Port, DM3_Pin, GPIO_PIN_SET);
		break;
	default:
		break;
	}
}
