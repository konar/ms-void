/*
 * encoders.h
 *
 *  Created on: 14 lut 2017
 *      Author: Aleksander Sil
 */

#ifndef SENSORS_ENCODERS_H_
#define SENSORS_ENCODERS_H_

#include "essentials.h"
#include "../Src/motor_control/motor_control.h"

#define ENCODER_ImpulsePerRevolution 2048.0f
#define ENC_DATA2RPS 1000.0f/ENCODER_ImpulsePerRevolution
#define	MOTOR_SPEED2WHEEL_SPEED (1.0f/19.0f)

void f_Encoders_Init();
void f_Encoders_GetValue();

#endif /* SENSORS_ENCODERS_H_ */
