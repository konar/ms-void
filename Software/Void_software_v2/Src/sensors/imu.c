/*
 * imu.c
 *
 *  Created on: 6 lut 2017
 *      Author: Aleksander Sil
 */

#include "imu.h"

#define FS_G_245 	(0b00 << 3)
#define FS_G_500 	(0b01 << 3)
#define FS_G_2000 	(0b11 << 3)

#define FS_XL_2		(0b00 << 3)
#define FS_XL_4		(0b10 << 3) // 00010 -> 10000
#define FS_XL_8		(0b11 << 3)
#define FS_XL_16	(0b01 << 3)

IMU_Data imu_data;
//uint8_t imu_data_ready = 0;

uint8_t IMU_AG_Read(uint8_t REG)
{
	uint8_t Data = 5;
	HAL_I2C_Mem_Read(&hi2c2, LSM9DS1_AG_ADDRESS, REG, 1, &Data, sizeof(Data), 100);
	return Data;
}

uint8_t IMU_M_Read(uint8_t REG)
{
	uint8_t Data = 5;
	HAL_I2C_Mem_Read(&hi2c2, LSM9DS1_M_ADDRESS, REG, 1, &Data, sizeof(Data), 100);
	return Data;
}

uint8_t IMU_AG_Write(uint8_t REG, uint8_t Data)
{
	if(HAL_I2C_Mem_Write(&hi2c2, LSM9DS1_AG_ADDRESS, REG, 1, &Data, sizeof(Data), 100) != HAL_OK){
		return HAL_ERROR;
	}
	return HAL_OK;
}

uint8_t IMU_M_Write(uint8_t REG, uint8_t Data)
{
	if(HAL_I2C_Mem_Write(&hi2c2, LSM9DS1_M_ADDRESS, REG, 1, &Data, sizeof(Data), 100) != HAL_OK){
		return HAL_ERROR;
	}
	return HAL_OK;
}

uint8_t IMU_Init(){
	uint8_t Error = 0;

	if(HAL_OK != IMU_AG_Write(AG_CTRL_REG1_G, 0b11000000 | FS_G_500)) Error++;
	if(HAL_OK != IMU_AG_Write(AG_CTRL_REG3_G, 0b01000000)) Error++; // High-pass filter
	if(HAL_OK != IMU_AG_Write(AG_CTRL_REG4_G, 0b00111000)) Error++; // Gyro axis enable

	if(HAL_OK != IMU_AG_Write(AG_CTRL_REG5_XL, 0b00111000)) Error++;
	if(HAL_OK != IMU_AG_Write(AG_CTRL_REG6_XL, 0b11000000 | FS_XL_4)) Error++;
	if(HAL_OK != IMU_AG_Write(AG_CTRL_REG7_XL, 0b10000000)) Error++; // filtr dolnoprzepustowy
//	if(HAL_OK != IMU_AG_Write(AG_CTRL_REG8_XL, 0b00000000)) Error++;
	if(HAL_OK != IMU_AG_Write(AG_CTRL_REG8, 0b00000100)) Error++;
	if(HAL_OK != IMU_AG_Write(AG_CTRL_REG9, 0b00000000)) Error++;


	// Interrupts config
	if(HAL_OK != IMU_AG_Write(INT_GEN_CFG_XL, 0b00101010)) Error++;
//	if(HAL_OK != IMU_AG_Write(INT_GEN_CFG_XL, 0b00101010)) Error++;
	if(HAL_OK != IMU_AG_Write(INT1_CTRL, 0b00000001)) Error++; // Gyroscope data ready interrupt on pin INT1_AG
	if(HAL_OK != IMU_AG_Write(INT2_CTRL, 0b00000001)) Error++; // Accelerometer data ready interrupt on pin INT2_AG

	imu_data.LinAcc_raw[0] = 0; imu_data.LinAcc_raw[1] = 0; imu_data.LinAcc_raw[2] = 0;
	imu_data.AngVel_raw[0] = 0; imu_data.AngVel_raw[1] = 0; imu_data.AngVel_raw[2] = 0;

	imu_data.LinAcc[0] = 0; imu_data.LinAcc[1] = 0; imu_data.LinAcc[2] = 0;
	imu_data.AngVel[0] = 0; imu_data.AngVel[1] = 0; imu_data.AngVel[2] = 0;


	// Ustawienia dokladnosci i  orientacji:
	if(HAL_OK != IMU_AG_Write(AG_ORIENT_CFG_G, 0b00001000)) Error++;

	return Error;
}

uint8_t IMU_Test(){
	if (	(IMU_AG_Read(AG_WHO_AM_I) == AG_WHO_AM_I_RETURN) &&\
			(IMU_M_Read(M_WHO_AM_I) == M_WHO_AM_I_RETURN)	){
		return 1;	}
	return 0;
}

uint8_t IMU_Acc_GetRawData(){
	uint8_t error = 0;
//	if(HAL_I2C_Mem_Read(&hi2c2, LSM9DS1_AG_ADDRESS, AG_ACC_X_L, 1, imu_data.LinAcc_raw, 6, 1) != HAL_OK) error++;
//	if(HAL_I2C_Mem_Read(&hi2c2, LSM9DS1_AG_ADDRESS, AG_ACC_Y_L, 1, &imu_data.LinAcc_raw[1], 2, 1) != HAL_OK) error++;
//	if(HAL_I2C_Mem_Read(&hi2c2, LSM9DS1_AG_ADDRESS, AG_ACC_Z_L, 1, &imu_data.LinAcc_raw[2], 2, 1) != HAL_OK) error++;
	return error;
}


uint8_t IMU_Gyr_GetRawData(){
	uint8_t error = 0;
//	if(HAL_I2C_Mem_Read(&hi2c2, LSM9DS1_AG_ADDRESS, AG_GYR_X_L, 1, &imu_data.AngVel_raw[0], 2, 5) != HAL_OK) error++;
//	if(HAL_I2C_Mem_Read(&hi2c2, LSM9DS1_AG_ADDRESS, AG_GYR_Y_L, 1, &imu_data.AngVel_raw[1], 2, 5) != HAL_OK) error++;
//	if(HAL_I2C_Mem_Read(&hi2c2, LSM9DS1_AG_ADDRESS, AG_GYR_Z_L, 1, &imu_data.AngVel_raw[2], 2, 5) != HAL_OK) error++;
	return error;
}

void IMU_HandleData(){
	uint8_t it = 0;
//	uint16_t gyr_full_scale = 500; // dps
	uint16_t acc_full_scale = 4; // g

	for(it = 0; it < 3; it++){
		imu_data.LinAcc[it] = ((float)imu_data.LinAcc_raw[it] / 65536.0) * acc_full_scale;
//		imu_data.AngVel[it] = ((float)imu_data.AngVel_raw[it] / 65536.0) * gyr_full_scale;
	}
	imu_data.LinAcc[0] *= -1;
	imu_data.LinAcc[1] *= -1;
	imu_data.LinAcc[2] *= -1;
}
