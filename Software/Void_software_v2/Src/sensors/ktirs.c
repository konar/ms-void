/*
 * ktirs.c
 *
 *  Created on: 14 lut 2017
 *      Author: Aleksander Sil
 */


#include "ktirs.h"
ktiry KTIRS;


void KTIR_Init()
{
	HAL_ADC_Start_DMA(&hadc3, (uint32_t *) KTIRS.Data, 4);

	KTIRS.Treshold[0] = 3000;
	KTIRS.Treshold[1] = 3000;
	KTIRS.Treshold[2] = 3000;
	KTIRS.Treshold[3] = 3000;

	KTIRS.Memory[0] = 0;
	KTIRS.Memory[1] = 0;
}

void KTIR_Update()
{
	if(KTIRS.Data[0] < KTIRS.Treshold[0] || KTIRS.Data[1] < KTIRS.Treshold[1])
		KTIRS.Memory[0] = 1;
	else
		KTIRS.Memory[0] = 0;

	if(KTIRS.Data[2] < KTIRS.Treshold[2] || KTIRS.Data[3] < KTIRS.Treshold[3])
		KTIRS.Memory[1] = 1;
	else
		KTIRS.Memory[1] = 0;

}

/*
 * Je�li najechales oboma, uciekaj, chyba ze przeciwnik z przodu
 * Ucieczka - nie sprawdzaj ktir�w, zresetuj i nie sprawdzaj sharpow przez moment
 * najlepiej na delayu.
 *
 * Jesli jednym:
 * przeciwnik po stronie czujnika - oslab silnik po stronie czujnika, max moment na przeciwnym
 * przeciwnik po przeciwnej stronie - oslab silnik po stronie czujnika. troche oslab po stronie przeciwnika
 * brak przeciwnika - mocno zwolnij, cofnij si�, obr�c sie i odjedz
 *
 * Mozna to zrobic na skalowaniu predkosci na silnikach �eby nastawy by�y odpowiednie do sytuacji.
 *
 * Zaimplementowac licznik do czegos - wymyslic :P
 *
 */

