/*
 * VL53L0X.h
 *
 *  Created on: xx lut 2017
 *      Author: Hubert Grzegorczyk
 *		Modified: Aleksander Sil
 */

#ifndef VL53L0X_VL53L0X_H_
#define VL53L0X_VL53L0X_H_

#include "vl53l0x_stm_api.h"
#include "../communication/expander.h"

#define VL_DEFAULT_ADDRESS 0x52
#define VL_DEV_NUMBER 6

//const int MAX_DISTANCE = 400; // [mm]

typedef struct{
	VL53L0X_DEV Device;
	uint8_t ID;
} VL_Sensor;
extern VL_Sensor VL_List[];

extern uint8_t VL_Data_Updated[];

typedef struct {
	int16_t STATE[6]; //= {GPIO_PIN_RESET, GPIO_PIN_RESET, GPIO_PIN_RESET, GPIO_PIN_RESET};
	int16_t MEMORY[6];
	int16_t MEMORY_OLD[6];
	int8_t TARGET_DIR;
	int8_t TARGET_DIR_OLD;
/*
 * Kolejnosc: L, FL, ML, MR, FR, L
 */
} vle;
extern vle VLS;

extern uint8_t VL_Data_Updated[6];

uint8_t VL_Init_All();

uint8_t VL_Check();

void VL_ClearMemory(int16_t* MEM);

uint8_t sNone(int16_t* MEM);

uint8_t sLeft(int16_t* MEM);
uint8_t sLeft_FrontL(int16_t* MEM);
uint8_t sLeft_MidL(int16_t* MEM);
uint8_t sLeft_MidL_FrontL(int16_t* MEM);
uint8_t sFrontL_MidL(int16_t* MEM);
uint8_t sMidL(int16_t* MEM);
uint8_t sFrontL(int16_t* MEM);

uint8_t sRight(int16_t* MEM);
uint8_t sRight_FrontR(int16_t* MEM);
uint8_t sRight_MidR(int16_t* MEM);
uint8_t sRight_MidR_FrontR(int16_t* MEM);
uint8_t sFrontR_MidR(int16_t* MEM);
uint8_t sMidR(int16_t* MEM);
uint8_t sFrontR(int16_t* MEM);

uint8_t sFrontL_MidL_Front_R(int16_t* MEM);
uint8_t sFrontL_MidR_Front_R(int16_t* MEM);
uint8_t sMidL_MidR(int16_t* MEM);
uint8_t sFrontL_MidL_MidR_FrontR(int16_t* MEM);
uint8_t sFrontL_FrontR(int16_t* MEM);

#endif /* VL53L0X_VL53L0X_H_ */
