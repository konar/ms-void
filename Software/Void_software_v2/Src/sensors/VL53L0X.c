/*
 * VL53L0X.c
 *
 *  Created on: 19 lut 2017
 *      Author: Aleksander Sil
 */

#ifndef VL53L0X_VL53L0X_C_
#define VL53L0X_VL53L0X_C_

#include "VL53L0X.h"

volatile uint8_t VL_Data_Ready[6] = {0,0,0,0,0,0};


void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)
{
	if(GPIO_Pin == INT1_Pin){
		VL_Data_Ready[0] = 1;
	}
	else if(GPIO_Pin == INT2_Pin){
		VL_Data_Ready[1] = 1;
	}
	else if(GPIO_Pin == INT3_Pin){
		VL_Data_Ready[2] = 1;
	}
	else if(GPIO_Pin == INT4_Pin){
		VL_Data_Ready[3] = 1;
	}
	else if(GPIO_Pin == INT5_Pin){
		VL_Data_Ready[4] = 1;
	}
	else if(GPIO_Pin == INT6_Pin){
		VL_Data_Ready[5] = 1;
	}
}


VL53L0X_Dev_t devs[VL_DEV_NUMBER];
VL_Sensor VL_List[VL_DEV_NUMBER];
vle VLS;
VL53L0X_DEV vlka;
uint8_t VL_Data_Updated[6] = {0,0,0,0,0,0};

uint8_t VL_Init_All(){
	uint8_t j = 0;

	Diode_VL_Set(1,0);

	HAL_Delay(1000);

	for(j=0; j<VL_DEV_NUMBER; j++){

		VL_List[j].Device = &devs[j];
		VL_List[j].Device->I2cDevAddr = VL_DEFAULT_ADDRESS;
		VL_List[j].Device->i2c_handle = &hi2c3;
		VL_List[j].ID = j;

		VLS.MEMORY[j] = -1;
		VLS.STATE[j] = -1;

		VL_Enable(j);
		VL_init_sensor_set_addres(VL_List[j].Device, (VL_DEFAULT_ADDRESS + j*2 + 2));
		VL_init_parameters(VL_List[j].Device, HIGH_SPEED, VL53L0X_DEVICEMODE_CONTINUOUS_RANGING);
		VL_set_interrupt(VL_List[j].Device, 1, VL53L0X_INTERRUPTPOLARITY_LOW);
		VL_start_measurement_non_blocking(VL_List[j].Device);
	}

	Diode_VL_Set(1,1);
	return 1;
}

uint8_t VL_Check()
{
	int j;

	for(j=0; j<6; j++)
	{
		if(VL_Data_Ready[j]) {
			VL_Data_Ready[j] = 0;
			VL_get_measurement(VL_List[j].Device, &VLS.STATE[j]);

			if(VLS.STATE[j] > 400) VLS.STATE[j] = -1;

			Diode_Set(j,(VLS.STATE[j] > 0 ? 1 : 0));
			VL_Data_Updated[j] = 1;
		}
	}


	if(VLS.STATE[0]>0 || VLS.STATE[1]>0 || VLS.STATE[2]>0 || \
			VLS.STATE[3]>0 || VLS.STATE[4]>0 || VLS.STATE[5]>0 )
	{
		for(j=0; j<6; j++) {
			VLS.MEMORY[j] = VLS.STATE[j];
			if(VLS.MEMORY[j] == VLS.MEMORY_OLD[j])
				VLS.MEMORY_OLD[j] = VLS.MEMORY[j];
		}
	}
	int8_t Temp = VLS.TARGET_DIR;

	VLS.TARGET_DIR = VLS.MEMORY[0]*(-4) + VLS.MEMORY[1]*(-1) + VLS.MEMORY[2]*(-2) + \
			VLS.MEMORY[3]*2 + VLS.MEMORY[4]*1 + VLS.MEMORY[5]*4;

	if(VLS.TARGET_DIR != Temp)
		VLS.TARGET_DIR_OLD = Temp;

	return 0;
}

void f_SH_ClearMemory(int16_t* MEM)
{
	uint8_t i = 0;
	for (i=0; i<6; i++)
	{
		MEM[i] = -1;
	}
}

uint8_t sNone(int16_t* MEM)
{
	if(MEM[0]==-1 && MEM[1]==-1 && MEM[2]==-1 \
			&& MEM[3]==-1 && MEM[4]==-1 && MEM[5]==-1)
	{
		return 1;
	}
	return 0;
}

uint8_t sLeft(int16_t* MEM)
{
	if(MEM[0]>=0 && MEM[1]==-1 && MEM[2]==-1 \
			&& MEM[3]==-1 && MEM[4]==-1 && MEM[5]==-1)
	{
		return 1;
	}
	return 0;
}

uint8_t sLeft_FrontL(int16_t* MEM)
{
	if(MEM[0]>=0 && MEM[1]>=0 && MEM[2]==-1 \
			&& MEM[3]==-1 && MEM[4]==-1 && MEM[5]==-1)
	{
		return 1;
	}
	return 0;
}

uint8_t sLeft_MidL(int16_t* MEM)
{
	if(MEM[0]>=0 && MEM[1]==-1 && MEM[2]>=0 \
			&& MEM[3]==-1 && MEM[4]==-1 && MEM[5]==-1)
	{
		return 1;
	}
	return 0;
}

uint8_t sLeft_Other(int16_t* MEM)
{	if(MEM[0]>=0)
	{	return 1;	}
	return 0;
}

uint8_t sLeft_MidL_FrontL(int16_t* MEM)
{
	if(MEM[0]>=0 && MEM[1]>=0 && MEM[2]>=0 \
			&& MEM[3]==-1 && MEM[4]==-1 && MEM[5]==-1)
	{
		return 1;
	}
	return 0;
}

uint8_t sFrontL_MidL(int16_t* MEM)
{
	if(MEM[0]==-1 && MEM[1]>=0 && MEM[2]>=0 \
			&& MEM[3]==-1 && MEM[4]==-1 && MEM[5]==-1)
	{
		return 1;
	}
	return 0;
}

uint8_t sMidL(int16_t* MEM)
{	if(MEM[0]==-1 && MEM[1]==-1 && MEM[2]>=0 \
			&& MEM[3]==-1 && MEM[4]==-1 && MEM[5]==-1)
	{	return 1;	}
	return 0;
}

uint8_t sFrontL(int16_t* MEM)
{	if(MEM[0]==-1 && MEM[1]>=0 && MEM[2]==-1 \
			&& MEM[3]==-1 && MEM[4]==-1 && MEM[5]==-1)
	{	return 1;	}
	return 0;
}

/*
 * Right
 */

uint8_t sRight(int16_t* MEM)
{	if(MEM[0]==-1 && MEM[1]==-1 && MEM[2]==-1 \
			&& MEM[3]==-1 && MEM[4]==-1 && MEM[5]>=0)
	{	return 1;	}
	return 0;
}

uint8_t sRight_FrontR(int16_t* MEM)
{	if(MEM[0]==-1 && MEM[1]==-1 && MEM[2]==-1 \
			&& MEM[3]==-1 && MEM[4]>=0 && MEM[5]>=0)
	{	return 1;	}
	return 0;
}
uint8_t sRight_MidR(int16_t* MEM)
{	if(MEM[0]==-1 && MEM[1]==-1 && MEM[2]==-1 \
			&& MEM[3]>=0 && MEM[4]==-1 && MEM[5]>=0)
	{	return 1;	}
	return 0;
}

uint8_t sRight_Other(int16_t* MEM)
{	if(MEM[5]>=0)
	{	return 1;	}
	return 0;
}

uint8_t sRight_MidR_FrontR(int16_t* MEM)
{	if(MEM[0]==-1 && MEM[1]==-1 && MEM[2]==-1 \
			&& MEM[3]>=0 && MEM[4]>=0 && MEM[5]>=0)
	{	return 1;	}
	return 0;
}

uint8_t sFrontR_MidR(int16_t* MEM)
{	if(MEM[0]==-1 && MEM[1]==-1 && MEM[2]==-1 \
			&& MEM[3]>=0 && MEM[4]>=0 && MEM[5]==-1)
	{	return 1;	}
	return 0;
}

uint8_t sMidR(int16_t* MEM)
{	if(MEM[0]==-1 && MEM[1]==-1 && MEM[2]==-1 \
			&& MEM[3]>=0 && MEM[4]==-1 && MEM[5]==-1)
	{	return 1;	}
	return 0;
}

uint8_t sFrontR(int16_t* MEM)
{	if(MEM[0]==-1 && MEM[1]==-1 && MEM[2]==-1 \
			&& MEM[3]==-1 && MEM[4]>=0 && MEM[5]==-1)
	{	return 1;	}
	return 0;
}

/*
 * Fronts
 */

uint8_t sFrontL_MidL_Front_R(int16_t* MEM)
{	if(MEM[0]==-1 && MEM[1]>=0 && MEM[2]>=0 \
			&& MEM[3]==-1 && MEM[4]>=0 && MEM[5]==-1)
	{	return 1;	}
	return 0;
}

uint8_t sFrontL_MidR_Front_R(int16_t* MEM)
{	if(MEM[0]==-1 && MEM[1]>=0 && MEM[2]==-1 \
			&& MEM[3]>=0 && MEM[4]>=0 && MEM[5]==-1)
	{	return 1;	}
	return 0;
}

uint8_t sMidL_MidR(int16_t* MEM)
{	if(MEM[0]==-1 && MEM[1]==-1 && MEM[2]>=0 \
			&& MEM[3]>=0 && MEM[4]==-1 && MEM[5]==-1)
	{	return 1;	}
	return 0;
}

uint8_t sFrontL_MidL_MidR_FrontR(int16_t* MEM)
{	if(MEM[0]==-1 && MEM[1]>=0 && MEM[2]>=0 \
			&& MEM[3]>=0 && MEM[4]>=0 && MEM[5]==-1)
	{	return 1;	}
	return 0;
}

uint8_t sFrontL_FrontR(int16_t* MEM)
{	if(MEM[0]==-1 && MEM[1]>=0 && MEM[2]==-1 \
			&& MEM[3]==-1 && MEM[4]>=0 && MEM[5]==-1)
	{	return 1;	}
	return 0;
}

#endif /* VL53L0X_VL53L0X_C_ */
