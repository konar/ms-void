/*
 * encoders.c
 *
 *  Created on: 14 lut 2017
 *      Author: Aleksander Sil
 */

#include "encoders.h"

void f_Encoders_Init()
{
	HAL_TIM_Encoder_Start(&htim2, TIM_CHANNEL_ALL);
	TIM2->CNT = 32000;

	HAL_TIM_Encoder_Start(&htim3, TIM_CHANNEL_ALL);
	TIM3->CNT = 32000;
}

void f_Encoders_GetValue()
{
	MOTORS.MT_Enc_data[0] = 32000 - TIM2->CNT;
	MOTORS.MT_Enc_data[1] = TIM3->CNT - 32000;

	TIM2->CNT = 32000;
	TIM3->CNT = 32000;
}
