/*
 * imu.h
 *
 *  Created on: 6 lut 2017
 *      Author: Aleksander Sil
 */

#ifndef SENSORS_IMU_H_
#define SENSORS_IMU_H_

#include "defines.h"
#include "lsm9ds1_def.h"

extern I2C_HandleTypeDef hi2c2;
extern uint8_t Address;

typedef struct{
	int16_t LinAcc_raw[3];
	int16_t AngVel_raw[3];

	float LinAcc[3];
	float AngVel[3];

} IMU_Data;
extern IMU_Data imu_data;
//extern uint8_t imu_data_ready;
//
//typedef enum {
//	NO_ERROR = 0,
//	TIMEOUT
//} Error_t;

uint8_t IMU_AG_Read(uint8_t);
uint8_t IMU_M_Read(uint8_t REG);
uint8_t IMU_AG_Write(uint8_t REG, uint8_t Data);
uint8_t IMU_M_Write(uint8_t REG, uint8_t Data);
uint8_t IMU_Init();
uint8_t IMU_Test();
uint8_t IMU_Acc_GetRawData();
uint8_t IMU_Gyr_GetRawData();
void IMU_HandleData();


#endif /* SENSORS_IMU_H_ */
