/*
 * ktirs.h
 *
 *  Created on: 14 lut 2017
 *      Author: Aleksander Sil
 */

#ifndef SENSORS_KTIRS_H_
#define SENSORS_KTIRS_H_

#include "essentials.h"

typedef struct {
	uint16_t Data[4];
	uint16_t Treshold[4];
	uint8_t Memory[2];
} ktiry; // KT
extern ktiry KTIRS;

void KTIR_Init();
void KTIR_Update();


#endif /* SENSORS_KTIRS_H_ */
