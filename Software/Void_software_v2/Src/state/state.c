/*
 * state.c
 *
 *  Created on: 15 lut 2017
 *      Author: Aleksander Sil
 */

#include "state.h"

uint8_t PI_control = 0;

void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim){
	if(htim->Instance == TIM7)
	{
			Cycle_time_elapsed = 1;
			if(!Cycle_finished)
			{
				Runtime_Error = TIMEOUT;
				Runtime_Error_Sum = Runtime_Error_Sum + 1;
			}
			else if (Cycle_finished)
			{
				Cycle_finished = 0;
				Runtime_Error = 0;
			}
	}
	else if(htim->Instance == TIM6)
	{
		perf_meas_ms++;

		f_Encoders_GetValue();
		MOTOR_CONTROL.Velocity_Measured[0] = MOTORS.MT_Enc_data[0]*ENC_DATA2RPS;
		MOTOR_CONTROL.Velocity_Measured[1] = MOTORS.MT_Enc_data[1]*ENC_DATA2RPS;

		if(PI_control) Velocity_Control();
	}
}


state STATE;
volatile Error_t Runtime_Error;
volatile uint16_t Runtime_Error_Sum = 0;
volatile uint8_t Cycle_time_elapsed = 0;
volatile uint8_t Cycle_finished = 0;
volatile uint16_t Cycle_counter = 0; // (0 - 100)
uint16_t PWM_Set[2];
uint8_t LED_mode_select = 4;
uint8_t Function_select = 1;




//f_Encoders_GetValue();
//
//MOTOR_CONTROL.Velocity_Measured[0] = MOTORS.MT_Enc_data[0]*ENC_DATA2RPS;
//MOTOR_CONTROL.Velocity_Measured[1] = MOTORS.MT_Enc_data[1]*ENC_DATA2RPS;


void State_Init()
{
	STATE.Motor_Current_Avg[0] = 0;
	STATE.Motor_Current_Avg[1] = 0;
	STATE.Motor_Velocity[0] = 0;
	STATE.Motor_Velocity[1] = 0;

	STATE.Batt_Voltage = 16.8;
	Cycle_time_elapsed = 0;
	Cycle_finished = 0;

	Diodes_Menu_Reset();
	Diodes_Reset();

	IK_Init();
	Expander_Init();
	KTIR_Init();
//	IMU_Init();

	HAL_Delay(500);
	VL_Init_All();

	f_MT_Init();
	f_Encoders_Init();
}

void State_Update(uint8_t Mode, uint8_t LED_Mode)
{
	/*	Cycle synchronizing	*/
	// 1000 Hz cycle
	if(HAL_TIM_Base_GetState(&htim7) == HAL_TIM_STATE_READY)
		HAL_TIM_Base_Start_IT(&htim7);

	if(HAL_TIM_Base_GetState(&htim6) == HAL_TIM_STATE_READY){
		HAL_TIM_Base_Start_IT(&htim6);
	}

	perf_avg_ms = (double)( perf_count*perf_avg_ms + perf_meas_ms ) / (double)(perf_count + 1);
	perf_meas_ms = 0;
	perf_count++;

	Cycle_finished = 1;
	Cycle_counter++;

	while(!Cycle_time_elapsed)	{}
	Cycle_time_elapsed = 0;

	/*
	 * Odczytaj dane pomiaru predkosci i pradu na silnikach
	 */



//	MOTOR_CONTROL.Velocity_Filtered[0] = (1-MOTOR_VELOCITY_FILTER_ALPHA)*MOTOR_CONTROL.Velocity_Measured[0] +\
//				MOTOR_VELOCITY_FILTER_ALPHA*MOTOR_CONTROL.Velocity_Previous[0];
//		MOTOR_CONTROL.Velocity_Previous[0] = MOTOR_CONTROL.Velocity_Measured[0];
//	MOTOR_CONTROL.Velocity_Filtered[1] = (1-MOTOR_VELOCITY_FILTER_ALPHA)*MOTOR_CONTROL.Velocity_Measured[1] +\
//				MOTOR_VELOCITY_FILTER_ALPHA*MOTOR_CONTROL.Velocity_Previous[1];
//		MOTOR_CONTROL.Velocity_Previous[1] = MOTOR_CONTROL.Velocity_Measured[1];
//
//	calculate_velocity(	MOTOR_CONTROL.Velocity_Measured[0]*MOTOR_SPEED2WHEEL_SPEED,
//						MOTOR_CONTROL.Velocity_Measured[1]*MOTOR_SPEED2WHEEL_SPEED);
//
//	MOTOR_CONTROL.Current_Measured[0] = MOTORS.MT_C_data[0]*C_DATA2CURRENT;
//	MOTOR_CONTROL.Current_Measured[1] = MOTORS.MT_C_data[1]*C_DATA2CURRENT;
//
//	MOTOR_CONTROL.Current_Filtered[0] = (1-MOTOR_CURRENT_FILTER_ALPHA)*MOTOR_CONTROL.Current_Measured[0] +\
//			MOTOR_CURRENT_FILTER_ALPHA*MOTOR_CONTROL.Current_Previous[0];
//	MOTOR_CONTROL.Current_Previous[0] = MOTOR_CONTROL.Current_Filtered[0];
//	MOTOR_CONTROL.Current_Filtered[1] = (1-MOTOR_CURRENT_FILTER_ALPHA)*MOTOR_CONTROL.Current_Measured[1] +\
//			MOTOR_CURRENT_FILTER_ALPHA*MOTOR_CONTROL.Current_Previous[1];
//	MOTOR_CONTROL.Current_Previous[1] = MOTOR_CONTROL.Current_Filtered[1];

	/*
	 * Zr�b odczyt czujnik�w
	 */



	VL_Check();
	KTIR_Update();
//	State_LEDs_SetMode(LED_Mode);
//	IMU_Acc_GetRawData();
//	IMU_HandleData();


	switch(Mode)
	{
		case 0:
			PI_control = 0;
			f_MT_SetSpeed(0, 0);
			break;
		case 1:
			PI_control = 1;
			break;
		case 2:
//			Velocity_Control();
			PI_control = 0;
			break;
		case 10: // Test
			PI_control = 0;
			f_MT_Update2(700,700);
			break;
		default:
			PI_control = 0;
			break;
	}
}

void State_StopCycle()
{
	HAL_TIM_Base_Stop_IT(&htim7);
	Cycle_time_elapsed = 0;
	Cycle_finished = 0;
}

/*
 * 0 - nothing
 * 1 - SHARP MEMORY
 * 2 - SHARP STATE
 * 3 - KTIR MEMORY
 * 4 - KTIR
 */
//TODO Do aktualizacji
void State_LEDs_SetMode(uint8_t Mode)
{
	uint8_t i = 0;
	switch(Mode)
	{
	default:
		break;
	case 0:
		break;
	case 1:
//		HAL_GPIO_WritePin(IK.diodyBus[1], IK.diodyPin[1], SHARPS.MEMORY[1]);
//		HAL_GPIO_WritePin(IK.diodyBus[6], IK.diodyPin[6], SHARPS.MEMORY[4]);
//
//		HAL_GPIO_WritePin(IK.diodyBus[2], IK.diodyPin[2], SHARPS.MEMORY[2]);
//		HAL_GPIO_WritePin(IK.diodyBus[5], IK.diodyPin[5], SHARPS.MEMORY[3]);
//
//		HAL_GPIO_WritePin(IK.diodyBus[3], IK.diodyPin[3], SHARPS.MEMORY[0]);
//		HAL_GPIO_WritePin(IK.diodyBus[4], IK.diodyPin[4], SHARPS.MEMORY[5]);
		break;
	case 2:
//		HAL_GPIO_WritePin(IK.diodyBus[1], IK.diodyPin[1], SHARPS.STATE[1]);
//		HAL_GPIO_WritePin(IK.diodyBus[6], IK.diodyPin[6], SHARPS.STATE[4]);
//
//		HAL_GPIO_WritePin(IK.diodyBus[2], IK.diodyPin[2], SHARPS.STATE[2]);
//		HAL_GPIO_WritePin(IK.diodyBus[5], IK.diodyPin[5], SHARPS.STATE[3]);
//
//		HAL_GPIO_WritePin(IK.diodyBus[3], IK.diodyPin[3], SHARPS.STATE[0]);
//		HAL_GPIO_WritePin(IK.diodyBus[4], IK.diodyPin[4], SHARPS.STATE[5]);
		break;
	case 3:
//		HAL_GPIO_WritePin(IK.diodyBus[1], IK.diodyPin[1], KTIRS.Memory[0]);
//		HAL_GPIO_WritePin(IK.diodyBus[2], IK.diodyPin[2], KTIRS.Memory[0]);
//		HAL_GPIO_WritePin(IK.diodyBus[3], IK.diodyPin[3], KTIRS.Memory[0]);
//
//		HAL_GPIO_WritePin(IK.diodyBus[4], IK.diodyPin[4], KTIRS.Memory[1]);
//		HAL_GPIO_WritePin(IK.diodyBus[5], IK.diodyPin[5], KTIRS.Memory[1]);
//		HAL_GPIO_WritePin(IK.diodyBus[6], IK.diodyPin[6], KTIRS.Memory[1]);
		break;
	case 4:
//		HAL_GPIO_WritePin(IK.diodyBus[1], IK.diodyPin[1], 0);
//		HAL_GPIO_WritePin(IK.diodyBus[6], IK.diodyPin[6], 0);

		for(i = 0; i<4; i++)
		{
			if(KTIRS.Data[i] < KTIRS.Treshold[i])	HAL_GPIO_WritePin(IK.diodyBus[i+2], IK.diodyPin[i+2], 1);
			else HAL_GPIO_WritePin(IK.diodyBus[i+2], IK.diodyPin[i+2], 0);
		}
	}
}

