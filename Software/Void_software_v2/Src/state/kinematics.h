#ifndef STATE_KINEMATICS_H_
#define STATE_KINEMATICS_H_

#include "essentials.h"

// params
static float wheel_distance = 0.07764; // [m] distance between wheels
static float wheel_diameter = 0.0352; // [m]

typedef struct {
	float vel_L;
	float vel_R;

	float vel_ang;
	float vel_lin;
} velocity_t;

extern volatile velocity_t velocity;

void calculate_velocity(float freq_L, float freq_R);

#endif
