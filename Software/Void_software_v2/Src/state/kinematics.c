/*
 * kinematics.c
 *
 *  Created on: 10.10.2017
 *      Author: Aleksander Sil
 */

#include "kinematics.h"

volatile velocity_t velocity = {0, 0, 0, 0};

void calculate_velocity(float freq_L, float freq_R){

	velocity.vel_L = freq_L * 2 * M_PI * wheel_diameter;
	velocity.vel_R = freq_R * 2 * M_PI * wheel_diameter;



	velocity.vel_lin = (velocity.vel_L + velocity.vel_R)/2.0f;
	velocity.vel_ang = (velocity.vel_L - velocity.vel_R)/wheel_distance;
}
