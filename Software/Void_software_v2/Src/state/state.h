/*
 * state.h
 *
 *  Created on: 15 lut 2017
 *      Author: Aleksander Sil
 */

#ifndef STATE_STATE_H_
#define STATE_STATE_H_

#include "essentials.h"
#include "../Src/motor_control/motor_control.h"
#include "../Src/sensors/sensors.h"
#include "../Src/communication/IO.h"
#include "kinematics.h"
#include "../Src/communication/expander.h"

#define STATE_UPDATE_FREQUENCY 1000;
extern volatile uint8_t Cycle_time_elapsed;
extern volatile uint8_t Cycle_finished;
extern uint8_t LED_mode_select;
extern uint8_t Function_select;
extern volatile uint16_t Runtime_Error_Sum;

/*
 * HTIM7 [ 200 Hz ] - glowny timer cyklu pracy
 */

typedef struct {
	uint16_t Motor_Current[1000];
	uint16_t Motor_Current_Index;
} data;

typedef struct {
	uint16_t Motor_Current_Avg[2]; // mA
	float Motor_Velocity[2]; // obr/s
	float Batt_Voltage;
} state;
extern state STATE;

void State_Init();
void State_Update(uint8_t Mode, uint8_t LED_Mode);
void State_Send();
void State_StopCycle();
void State_LEDs_SetMode(uint8_t Mode);


#endif /* STATE_STATE_H_ */
