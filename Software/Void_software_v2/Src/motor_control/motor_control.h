/*
 * motor_control.h
 *
 *  Created on: 5 lut 2017
 *      Author: Aleksander Sil
 */

#ifndef MOTOR_CONTROL_H_
#define MOTOR_CONTROL_H_

#include "essentials.h"

#define GEAR_RATIO 19.0f
#define GEAR_EFFICIENCY 0.78f // percents
#define GEAR_BACKLASH 4.0f // max, degrees

#define C_DATA2CURRENT 0.493667f
#define C_VOLTAGE2SPEED 166.6667f // RPM / V

#define MAX_SPEED 999.0f
#define SPEED_RATIO (MAX_SPEED/999)

extern const float MOTOR_CURRENT_FILTER_ALPHA;
extern const float MOTOR_VELOCITY_FILTER_ALPHA;
/*
 * Struktura silnik�w
 * MT - port pin�w sterowania silnikami
 * MTL_F - stan pinu silnika lewego (prz�d)
 * MTL_B - stan pinu silnika lewego (ty�)
 * MTR_F - stan pinu silnika prawego (prz�d)
 * MTR_B - stan pinu silnika prawego (ty�)
 * a_MTL_F - adres pinu silnika lewego (prz�d)
 * a_MTL_B - adres pinu silnika lewego (ty�)
 * a_MTR_F - adres pinu silnika prawego (prz�d)
 * a_MTR_B - adres pinu silnika prawego (ty�)
 * MTL_Dty - wype�nienie PWMa lewego silnika
 * MTR_Dty - wype�nienie PWMa prawego silnika
 */
typedef struct {
	GPIO_TypeDef* a_MTL_F; //= ((GPIO_TypeDef *) GPIOB_BASE);
	GPIO_TypeDef* a_MTL_B;
	GPIO_TypeDef* a_MTR_F;
	GPIO_TypeDef* a_MTR_B;
	GPIO_PinState MTL_F;
	GPIO_PinState MTL_B;
	GPIO_PinState MTR_F;
	GPIO_PinState MTR_B;
	uint16_t p_MTL_F;
	uint16_t p_MTL_B;
	uint16_t p_MTR_F;
	uint16_t p_MTR_B;
	int16_t MTL_Dty;
	int16_t MTR_Dty;

	uint16_t MT_C_data[2];
	volatile int16_t MT_Enc_data[2];
	uint16_t PWM_Limit[2];

} silniki; // MT
extern silniki MOTORS;

typedef struct {
	float Velocity_Target[2]; // RPS
	float Velocity_Set[2]; // RPS
	float Velocity_Measured[2]; // RPS (na silniku)
	float Velocity_Previous[2];
	float Velocity_Filtered[2];

	float Current_Measured[2];
	float Current_Previous[2];
	float Current_Filtered[2];
} motor_data;
extern motor_data MOTOR_CONTROL;

/*
 * Inicjalizuje struktur� silnik�w
 * Ustawia domyslne wartosci PWMa na minimum, aby silniki nie ruszy� w momencie
 * uruchomienia/restartu robota
 */
void f_MT_Init();

typedef struct {
	float error;
	float error_sum;
	float error_sum_limit;

	float Kp;
	float Ki;
} PI_Data;
extern PI_Data PI_Data_Vel_L;
extern PI_Data PI_Data_Vel_R;

void Velocity_Control();
void Velocity_SetTarget(float L_Vel, float R_Vel);

/*
 * Funkcja ustalajaca aktualna predkosc silnikow
 * Wywolanie:
 * DutyL, DutyR - indeksy elementow z tablicy PWM�w zadeklarowanej w funkcji
 * LF, RF - przod [1, 0]
 * LB, RB - tyl [1, 0]
 */
void f_MT_Update(uint16_t, uint16_t, uint16_t, uint16_t, uint16_t, uint16_t);
void f_MT_Update2(int16_t, int16_t);
int16_t Velocity2PWM(float Velocity);
void f_MT_SetSpeed(int16_t, int16_t);



uint16_t m_ScaleSpeed(uint16_t);
void m_Forward(uint16_t);
void m_Reverse(uint16_t);
void m_StopLeft( uint16_t);
void m_StopRight( uint16_t);
void m_Left( uint16_t);
void m_Right( uint16_t);
void m_HardBreak();
void m_Cleaning();


#endif /* MOTOR_CONTROL_H_ */
