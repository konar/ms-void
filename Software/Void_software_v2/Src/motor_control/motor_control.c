/*
 * motor_control.c
 *
 *  Created on: 5 lut 2017
 *      Author: Aleksander Sil
 */

#include "motor_control.h"
silniki MOTORS;
motor_data MOTOR_CONTROL;
const float MOTOR_CURRENT_FILTER_ALPHA = 0.9; // [0, 1]
const float MOTOR_VELOCITY_FILTER_ALPHA = 0.9;

PI_Data PI_Data_Vel_L = { 0, 0, 500, 4, 0.2};
PI_Data PI_Data_Vel_R = { 0, 0, 500, 4, 0.2};
//PI_Data PI_Data_Vel_L = { 0, 0, 500, 6, 0};
//PI_Data PI_Data_Vel_R = { 0, 0, 500, 6, 0};

float PI(PI_Data* Data, float Target, float Measured_Value){

	Data->error = Target - Measured_Value;
	Data->error_sum = Data->error_sum + Data->error * Data->Ki;

	if(Data->error_sum > Data->error_sum_limit)
		Data->error_sum = Data->error_sum_limit;
	else if(Data->error_sum < -1*Data->error_sum_limit)
		Data->error_sum = -1*Data->error_sum_limit;

	return Data->error*Data->Kp + Data->error_sum;
}


void Velocity_Control()
{
//	if(fabs(MOTOR_CONTROL.Velocity_Target[0]) < 20) MOTOR_CONTROL.Velocity_Set[0] = 0;
	MOTOR_CONTROL.Velocity_Set[0] = PI(&PI_Data_Vel_L, MOTOR_CONTROL.Velocity_Target[0], MOTOR_CONTROL.Velocity_Measured[0]);

//	if(fabs(MOTOR_CONTROL.Velocity_Target[1]) < 20) MOTOR_CONTROL.Velocity_Set[1] = 0;
	MOTOR_CONTROL.Velocity_Set[1] = PI(&PI_Data_Vel_R, MOTOR_CONTROL.Velocity_Target[1], MOTOR_CONTROL.Velocity_Measured[1]);

	f_MT_SetSpeed(MOTOR_CONTROL.Velocity_Set[0], MOTOR_CONTROL.Velocity_Set[1]);
}

void Velocity_SetTarget(float L_Vel, float R_Vel)
{
	if(fabs(MOTOR_CONTROL.Velocity_Target[0] - L_Vel) < 1) PI_Data_Vel_L.error_sum = 0;
	if(fabs(MOTOR_CONTROL.Velocity_Target[1] - R_Vel) < 1) PI_Data_Vel_R.error_sum = 0;

	MOTOR_CONTROL.Velocity_Target[0] = L_Vel;
	MOTOR_CONTROL.Velocity_Target[1] = R_Vel;
}

void f_MT_Init()
{
	HAL_TIMEx_PWMN_Start(&htim1, TIM_CHANNEL_1);
	HAL_TIMEx_PWMN_Start(&htim1, TIM_CHANNEL_2);

	MOTORS.MTL_F=GPIO_PIN_RESET;
	MOTORS.MTL_B=GPIO_PIN_RESET;
	MOTORS.MTR_F=GPIO_PIN_RESET;
	MOTORS.MTR_B=GPIO_PIN_RESET;

	MOTORS.MTL_Dty=MAX_SPEED; // 0 - 99
	MOTORS.MTR_Dty=MAX_SPEED; // 0 - 99

	MOTORS.a_MTL_F=IN11_GPIO_Port; // 21 / 22
	MOTORS.a_MTL_B=IN12_GPIO_Port; // 22 / 21
	MOTORS.p_MTL_F=IN11_Pin; //21 / 22
	MOTORS.p_MTL_B=IN12_Pin; //22 / 21

	MOTORS.a_MTR_F=IN22_GPIO_Port; // 11 / 12
	MOTORS.a_MTR_B=IN21_GPIO_Port; // 12 / 11
	MOTORS.p_MTR_F=IN22_Pin; //11 / 12
	MOTORS.p_MTR_B=IN21_Pin; //12 / 11

	HAL_GPIO_WritePin(MOTORS.a_MTL_F,MOTORS.p_MTL_F,GPIO_PIN_RESET);
	HAL_GPIO_WritePin(MOTORS.a_MTL_B,MOTORS.p_MTL_B,GPIO_PIN_RESET);
	HAL_GPIO_WritePin(MOTORS.a_MTR_F,MOTORS.p_MTR_F,GPIO_PIN_RESET);
	HAL_GPIO_WritePin(MOTORS.a_MTR_B,MOTORS.p_MTR_B,GPIO_PIN_RESET);

	MOTORS.PWM_Limit[0] = 999; MOTORS.PWM_Limit[1] = 999;

	HAL_ADC_Start_DMA(&hadc2, (uint32_t *) MOTORS.MT_C_data, 2);

	MOTOR_CONTROL.Current_Filtered[0] = 0;
	MOTOR_CONTROL.Current_Filtered[1] = 0;
	MOTOR_CONTROL.Current_Previous[0] = 0;
	MOTOR_CONTROL.Current_Previous[1] = 0;
}

void f_MT_Update(uint16_t DutyL, uint16_t DutyR, uint16_t LF, uint16_t RF, uint16_t LB, uint16_t RB)
{
	if(DutyL > 999)	DutyL = 999;
	if(DutyR > 999)	DutyR = 999;

	MOTORS.MTL_Dty=999 - DutyL;//PWM_L[DutyL];
	MOTORS.MTR_Dty=999 - DutyR;//PWM_R[DutyR];

    TIM1->CCR1=MOTORS.MTL_Dty;
    TIM1->CCR2=MOTORS.MTR_Dty;

	if(LF==1)
		{
			HAL_GPIO_WritePin(MOTORS.a_MTL_F,MOTORS.p_MTL_F,GPIO_PIN_SET);
		}
		else
		{
			HAL_GPIO_WritePin(MOTORS.a_MTL_F,MOTORS.p_MTL_F,GPIO_PIN_RESET);
		}
	if(RF==1)
		{
			HAL_GPIO_WritePin(MOTORS.a_MTR_F,MOTORS.p_MTR_F,GPIO_PIN_SET);
		}
		else
		{
			HAL_GPIO_WritePin(MOTORS.a_MTR_F,MOTORS.p_MTR_F,GPIO_PIN_RESET);
		}
	if(LB==1)
		{
			HAL_GPIO_WritePin(MOTORS.a_MTL_B,MOTORS.p_MTL_B,GPIO_PIN_SET);
		}
		else
		{
			HAL_GPIO_WritePin(MOTORS.a_MTL_B,MOTORS.p_MTL_B,GPIO_PIN_RESET);
		}
	if(RB==1)
		{
			HAL_GPIO_WritePin(MOTORS.a_MTR_B,MOTORS.p_MTR_B,GPIO_PIN_SET);
		}
		else
		{
			HAL_GPIO_WritePin(MOTORS.a_MTR_B,MOTORS.p_MTR_B,GPIO_PIN_RESET);
		}
}

void f_MT_Update2(int16_t L_PWM, int16_t R_PWM)
{
	MOTORS.MTL_Dty = L_PWM;
	MOTORS.MTR_Dty = R_PWM;

	uint16_t L_Duty = fabsf(L_PWM);
	uint16_t R_Duty = fabsf(R_PWM);

	if(L_Duty > 999)	L_Duty = 999;
	if(R_Duty > 999)	R_Duty = 999;

	TIM1->CCR1 = 999 - L_Duty;
	TIM1->CCR2 = 999 - R_Duty;

	if(L_PWM >= 0)	HAL_GPIO_WritePin(MOTORS.a_MTL_F, MOTORS.p_MTL_F, GPIO_PIN_SET);
	else			HAL_GPIO_WritePin(MOTORS.a_MTL_F, MOTORS.p_MTL_F, GPIO_PIN_RESET);
	if(L_PWM <= 0)	HAL_GPIO_WritePin(MOTORS.a_MTL_B, MOTORS.p_MTL_B, GPIO_PIN_SET);
	else			HAL_GPIO_WritePin(MOTORS.a_MTL_B, MOTORS.p_MTL_B, GPIO_PIN_RESET);

	if(R_PWM >= 0)	HAL_GPIO_WritePin(MOTORS.a_MTR_F, MOTORS.p_MTR_F, GPIO_PIN_SET);
	else			HAL_GPIO_WritePin(MOTORS.a_MTR_F, MOTORS.p_MTR_F, GPIO_PIN_RESET);
	if(R_PWM <= 0)	HAL_GPIO_WritePin(MOTORS.a_MTR_B, MOTORS.p_MTR_B, GPIO_PIN_SET);
	else			HAL_GPIO_WritePin(MOTORS.a_MTR_B, MOTORS.p_MTR_B, GPIO_PIN_RESET);
}

int16_t Velocity2PWM(float Velocity)
{
	// (Velocity = (PWM/1000) * U_Batt * k_U)  -  k_U [RPM]
	// U_batt = 16.8
	// k_U = 1117 RPM/V = 18.617 RPS/V
	return (Velocity*1000)/(166.6667);
}

void f_MT_SetSpeed(int16_t SpeedL, int16_t SpeedR)
{
	f_MT_Update2(Velocity2PWM(SpeedL), Velocity2PWM(SpeedR));
	return;
}



uint16_t m_ScaleSpeed(uint16_t Speed)
	{

		if(Speed > MAX_SPEED)
			Speed = MAX_SPEED;
		else if (Speed <= 0 )
			Speed = 0;

		return (  Speed * SPEED_RATIO );
	}

void m_Forward(uint16_t speed)
	 {
		if (!TOURNAMENT)	speed = m_ScaleSpeed(speed);
		 f_MT_Update( speed, speed, 1, 1, 0, 0);
	 }

void m_Reverse(uint16_t speed)
	 {
		if (!TOURNAMENT)	if (!TOURNAMENT)	speed = m_ScaleSpeed(speed);
		f_MT_Update( speed, speed, 0, 0, 1, 1);
	 }

void m_StopLeft(uint16_t speed)
	 {
		if (!TOURNAMENT)	speed = m_ScaleSpeed(speed);
		f_MT_Update( speed, MAX_SPEED, 0, 1, 1, 0);
	 }

void m_StopRight(uint16_t speed)
	{
		if (!TOURNAMENT)	speed = m_ScaleSpeed(speed);
		f_MT_Update( MAX_SPEED, speed, 1, 0, 0, 1);
	}

void m_Left(uint16_t speed)
	{
		if (!TOURNAMENT)	speed = m_ScaleSpeed(speed);
		f_MT_Update( MAX_SPEED-speed, MAX_SPEED, 1, 1, 0, 0);
	}

void m_Right(uint16_t speed)
	{
		if (!TOURNAMENT)	speed = m_ScaleSpeed(speed);
		f_MT_Update( MAX_SPEED, MAX_SPEED-speed, 1, 1, 0, 0);
	}

void m_HardBreak()
	{
		f_MT_Update(MAX_SPEED, MAX_SPEED, 0, 0, 1, 1);
		HAL_Delay(80);
		f_MT_Update(0, 0, 0, 0, 0, 0);

	}

void m_Cleaning()
{
	m_Forward(250);
}



