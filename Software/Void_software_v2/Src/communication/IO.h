/*
 * IO.h
 *
 *  Created on: 14 lut 2017
 *      Author: Aleksander Sil
 */

#ifndef COMMUNICATION_IO_H_
#define COMMUNICATION_IO_H_

#include "essentials.h"

typedef struct {
	//Przyciski
	GPIO_TypeDef* przyciskiBus[3];
	uint16_t przyciskiPin[3];

	//Diody
	GPIO_TypeDef* diodyBus[6];
	uint16_t diodyPin[6];

	GPIO_TypeDef* diodyMenuBus[3];
	uint16_t diodyMenuPin[3];
} inter_kom; // IK
extern inter_kom IK;

void IK_Init();

void Diodes_Reset();

void Diodes_Menu_Reset();

void Diode_Set(uint8_t, uint8_t);

void Diode_Menu_Set(uint8_t, uint8_t);

#endif /* COMMUNICATION_IO_H_ */
