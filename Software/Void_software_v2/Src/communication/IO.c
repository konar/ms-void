/*
 * IO.c
 *
 *  Created on: 14 lut 2017
 *      Author: Aleksander Sil
 */

#include "IO.h"
inter_kom IK;

void IK_Init() //
{
	IK.diodyBus[0] = D1_GPIO_Port;
	IK.diodyPin[0] = D1_Pin;

	IK.diodyBus[1] = D2_GPIO_Port;
	IK.diodyPin[1] = D2_Pin;

	IK.diodyBus[2] = D3_GPIO_Port;
	IK.diodyPin[2] = D3_Pin;

	IK.diodyBus[3] = D4_GPIO_Port;
	IK.diodyPin[3] = D4_Pin;

	IK.diodyBus[4] = D5_GPIO_Port;
	IK.diodyPin[4] = D5_Pin;

	IK.diodyBus[5] = D6_GPIO_Port;
	IK.diodyPin[5] = D6_Pin;

	IK.diodyMenuBus[0] = DM1_GPIO_Port;
	IK.diodyMenuPin[0] = DM1_Pin;

	IK.diodyMenuBus[1] = DM2_GPIO_Port;
	IK.diodyMenuPin[1] = DM2_Pin;

	IK.diodyMenuBus[2] = DM3_GPIO_Port;
	IK.diodyMenuPin[2] = DM3_Pin;

	Diodes_Reset();
	Diodes_Menu_Reset();
}

void Diodes_Reset()
{
	int i=0;
	for(i=0;i<6;i++){
		Diode_Set(i,0);
	}
}

void Diodes_Menu_Reset()
{
	int i=0;
	for(i=0;i<3;i++){
		Diode_Menu_Set(i,0);
	}
}

void Diode_Set(uint8_t index, uint8_t state)
{
	if(state==0)
		HAL_GPIO_WritePin(IK.diodyBus[index], IK.diodyPin[index], GPIO_PIN_SET);
	else
		HAL_GPIO_WritePin(IK.diodyBus[index], IK.diodyPin[index], GPIO_PIN_RESET);
}

void Diode_Menu_Set(uint8_t index, uint8_t state)
{
	if(state==0)
		HAL_GPIO_WritePin(IK.diodyMenuBus[index], IK.diodyMenuPin[index], GPIO_PIN_RESET);
	else
		HAL_GPIO_WritePin(IK.diodyMenuBus[index], IK.diodyMenuPin[index], GPIO_PIN_SET);
}
