/*
 * expander.c
 *
 *  Created on: 14 maj 2017
 *      Author: Aleksander Sil
 */

#include "expander.h"
uint8_t Data = 0;
uint8_t VL_mapping[6] = {2, 3, 0, 1, 4, 5}; // Konwersja z oznaczenia 0 - 5 od L do R
// na takie zeby pasowalo do funkcji expander_enable_VL tj. bardziej fizyczne
// oznaczenie.

void Expander_Init(){
	// [IODIR] Inicjalizacja kierunku port�w GPIO. 0 - output, 1 - input
	Data = 0b00000000; // 0 - 5 to enable VLi, 6 - 7 - Interrupt z VLi
	HAL_I2C_Mem_Write(&hi2c3, Expander_address, Expander_Reg_IODIR, 1, &Data, 1, 100);

	// [IPOL] Invert wartosci na pinach. Zostawic domyslne (0000 0000)

	// [GPINTEN] Interrupt-on-change. P�ki co zostawic wylaczone (0000 0000)

	// [DEFVAL] Porownanie wartosci z ta na pinie. Jesli stan pinu zmieni si�
	// z warto�ci DEFVAL na inn� to wysy�a interrupt. Zostawic (0000 0000)

	// [INTCON] Tryb porownania pinu dla przerwan. Jesli 1 to porownuje z DEFVAL
	// jesli 0 to porownuje z poprzednim stanem. Zostawic (0000 0000)

	// [IOCON] Ustawienia expandera.
	// (xx[SEQOP][DISSLW][HAEN][ODR][INTPOL]x)
	// (xx[0][0][0][0][0]x)

	// [GPPU] Pull-upy dla wejsc (0000 0000)

	// [INTF] Read-only. Rejestr pokazuje, czy zostalo wywolane przerwanie dla
	// jakiegos pinu.

	// [INTCAP] Interrupy capture. Zachowuje wartosc, ktora mial pin w momencie wywolania
	// przerwania. Czysci sie przez odczytanie tego rejestru lub odczytanie rejestru GPIO

	// [GPIO] Opisane w naglowku. Czytanie odczytuje wartosc, write nadpisuje rejestr OLAT

	// [OLAT] Read - odczytuje wartosc OLAT, Write ustawia pin.
	Data = 0b11000000;
	HAL_I2C_Mem_Write(&hi2c3, Expander_address, Expander_Reg_OLAT, 1, &Data, 1, 100);
	Data = 0;
}

void VL_Enable(uint8_t VL_Index){
	uint8_t vl_bit = 1;
	vl_bit = vl_bit << VL_mapping[VL_Index];

	if(HAL_I2C_Mem_Read(&hi2c3, Expander_address, Expander_Reg_OLAT, 1, &Data, 1, 2) == HAL_OK){
		Data |= vl_bit;
		HAL_I2C_Mem_Write(&hi2c3, Expander_address, Expander_Reg_OLAT, 1, &Data, 1, 2);
	}
}

void Diode_VL_Set(uint8_t diode1_state, uint8_t diode2_state){
	if(diode1_state) 	Data &= ~(1 << 6);
			else		Data |= 1 << 6;
	if(diode2_state) 	Data &= ~(1 << 7);
			else		Data |= 1 << 7;
	HAL_I2C_Mem_Write(&hi2c3, Expander_address, Expander_Reg_OLAT, 1, &Data, 1, 100);
}
