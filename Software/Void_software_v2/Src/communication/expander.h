/*
 * expander.h
 *
 *  Created on: 14 maj 2017
 *      Author: Aleksander Sil
 */

#ifndef COMMUNICATION_EXPANDER_H_
#define COMMUNICATION_EXPANDER_H_

#include "essentials.h"
#include "IO.h"

#define Expander_address 0b01000000
//#define Expander_address 64

#define Expander_Reg_IODIR 0x00 // Controls GPIO direction. 1 - input / 0 - output
#define Expander_Reg_IPOL 0x01 // Powoduje, �e GPIO b�dzie pokazywac odwrocona wartosc pinu
#define Expander_Reg_GPINTEN 0x02
#define Expander_Reg_DEFVAL 0x03
#define Expander_Reg_INTCON 0x04
#define Expander_Reg_IOCON 0x05
#define Expander_Reg_GPPU 0x06
#define Expander_Reg_INTF 0x07 // read only
#define Expander_Reg_INTCAP 0x08
#define Expander_Reg_GPIO 0x09 // Read: value on the port, Write: writes to OLAT instead
#define Expander_Reg_OLAT 0x0A // Read: read OLAT ?



void Expander_Init();
void VL_Enable(uint8_t VL_Index);
void Diode_VL_Set(uint8_t diode1_state, uint8_t diode2_state);


#endif /* COMMUNICATION_EXPANDER_H_ */
